import UIKit

class ExercircioTableViewCell: UITableViewCell {

    @IBOutlet weak var imageViewExercicio: UIImageView!
    @IBOutlet weak var lblNomeExercicio: UILabel!
    @IBOutlet weak var lblRepeticoes: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
