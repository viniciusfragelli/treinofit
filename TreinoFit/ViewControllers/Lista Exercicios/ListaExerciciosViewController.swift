import UIKit

class ListaExerciciosViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Treino Fit"
        setupTableView()
        setupNavegation()
    }
    
    private func setupNavegation(){
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(onClickAdd))
    }
    
    @objc private func onClickAdd(){
        let vc = NovoExercicioViewController(nibName: "NovoExercicioViewController", bundle: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func setupTableView(){
        tableView.register(UINib(nibName: "ExercircioTableViewCell", bundle: nil), forCellReuseIdentifier: "ExercircioTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

}

extension ListaExerciciosViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExercircioTableViewCell") as! ExercircioTableViewCell
        cell.lblNomeExercicio.text = "Teste de exercicio"
        cell.lblRepeticoes.text = "2 repetições"
        cell.imageViewExercicio.image = #imageLiteral(resourceName: "fit")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
